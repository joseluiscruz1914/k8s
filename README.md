# K8s
## K8s Local con Minikube
```
minikube start \
--profile=cloud-computing \
--memory=2048 \
--cpus=2 \
--disk-size=5g \
--kubernetes-version=v1.22.3 \
--driver=docker

minikube profile cloud-computing
```

## plugins minikube
- minikube addons enable ingress
- minikube addons enable metrics-server

```
kubectl get nodes

kubectl apply -f  deployment.yaml

kubectl get all

kubectl apply -f  nginx-service.yaml

kubectl get svc

minikube ip


kubectl delete pod --selector app=nginx
kubectl get pod --watch


kubectl run -i --rm --restart=Never curl-client --image=curlimages/curl --command -- curl -s 'http://nginx-service:80'
```


### Fin
- minikube delete --profile  cloud-computing


# K8s Cluster CloudComputing 

## agregar config local

- config a ./kube/config

### agregar a gitlab variable
- cat config | base64 > config.txt
- cat config.txt copiar a KUBE_CONFIG

#### CICI con k8s

- crear archivo .gitlab-ci.yml
```

stages:
  - deploy

deploy-k8s:
  stage: deploy
  image:
    name: bitnami/kubectl:latest
    entrypoint: [ "" ]
  before_script:
    - echo $KUBE_CONFIG | base64 -d > config
    - mv config /.kube/
  environment:
    name: cloudcomputing-$grupo
    url: http://k8s-lia.unrn.edu.ar/$grupo
    kubernetes:
      namespace: $grupo
  script:
    - kubectl config get-contexts
    - kubectl config use-context microk8s
    - kubectl config set-context --current --namespace=$grupo
    - kubectl get pods
    # hacer el deploy mediante kubectl 
  only:
    - main
```



# Certificados 

## agregar servicio cert-manager (Se hacer una sola vez, por el admin del cluster)
```
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.5.3 --set installCRDs=true  
```

##  agregar certificados, al namespace de cert-manager (Se hacer una sola vez, por el admin del cluster)

```
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-production
  namespace: cert-manager
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: hmunoz@unrn.edu.ar
    privateKeySecretRef:
       name: letsencrypt-production
    solvers:
    - http01:
        ingress:
          class: public
```

## modificaciones para agregar al ingress

```
 annotations:
    cert-manager.io/cluster-issuer: letsencrypt-production
```

```
 spec:
  tls:
    - hosts:
      - k8s-lia.unrn.edu.ar
      secretName: letsencrypt-production
```

